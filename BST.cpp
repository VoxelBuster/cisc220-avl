/*
 * BST.cpp
 *
 *  Created on: Oct 20, 2020
 *      Author: Galen Nare
 */

#include "BST.hpp"


BST::BST() {
	root = nullptr;
}
BST::BST(StudInf* s) {
	root = new TNode(s);
}

BST::BST(string sarr[]) {
	root = new TNode(new StudInf(sarr));
}

void BST::printTreeIO() {
	if (root == nullptr ) {
		cout << "Empty Tree" << endl;
	}
	else {
		cout << endl<<"Printing In Order:" <<endl;
		printTreeIO(root);
	}
}


void BST::printTreePre() {
	if (root == nullptr ) {
		cout << "Empty Tree" << endl;
	}
	else {
		cout << endl<<"Printing PreOrder:" <<endl;
		printTreePre(root);
	}
}


void BST::printTreePost() {
	if (root == nullptr ) {
		cout << "Empty Tree" << endl;
	}
	else {
		cout << endl<<"Printing PostOrder:" <<endl;
		printTreePost(root);
	}
}


void BST::clearTree() {
	if (root == nullptr) {
		cout << "Tree already empty" << endl;
	}
	else {
		cout << endl << "Clearing Tree:" << endl;
		clearTree(root);
		root = nullptr;
	}
}
void BST::clearTree(TNode *tmp) {
	if (tmp == nullptr) {
		return;
	}
	else {
		clearTree(tmp->left);
		clearTree(tmp->right);
		tmp->printNode();
		delete(tmp);
	}
}

TNode* BST::find(StudInf* data) {
	return find(data, root);
}

TNode* BST::find(string l, string f) {
	std::string sarr[6] = {l, "", "", f, "", ""};
	return find(new StudInf(sarr));
}

TNode* BST::find(StudInf* data, TNode* start) {
	if (start == nullptr) {
		return nullptr;
	}

	if (start->data->first == data->first && start->data->last == data->last) {
		return start;
	} else if (data->last.compare(start->data->last) < 0 || (data->last == start->data->last && data->first.compare(start->data->first) < 0 )) {
		return find(data, start->left);
	} else {
		return find(data, start->right);
	}
}

bool BST::insert(StudInf* data) {
	if (root == nullptr) {
		TNode* node = new TNode(data);
		root = node;
		setHeight();
		return true;
	}

	return insert(data, root);
}

bool BST::insert(string sarr[]) {
	return insert(new StudInf(sarr));
}

bool BST::insert(StudInf* data, TNode* start) {
	TNode* node = new TNode(data);
	
	if (start->data->first == data->first && start->data->last == data->last) {
		return false; // Don't insert duplicates
	} else if (data->last.compare(start->data->last) < 0 || (data->last == start->data->last && data->first.compare(start->data->first) < 0 )) {
		if (start->left == nullptr) {
			start->left = node;
			node->parent = start;
			setHeight();
			rebalance(node);
			return true;
		} else {
			return insert(data, start->left);
		}
	} else {
		if (start->right == nullptr) {
			start->right = node;
			node->parent = start;
			setHeight();
			rebalance(node);
			return true;
		} else {
			return insert(data, start->right);
		}
	}
}

int max(int a, int b) {
	if (a > b) {
		return a;
	} else {
		return b;
	}
}

int BST::setHeight(TNode* node) {
	if (node == nullptr) {
		return 0;
	}
	int m = max((node->left != nullptr) ? setHeight(node->left) : 0, (node->right != nullptr) ? setHeight(node->right) : 0);
	// if (root == node) {
	// 	std::cout << m;
	// 	std::cout << std::endl;
	// }
	node->height = (1 + m);
	return node->height;
}

int BST::setHeight() {
	return setHeight(root);
}

void BST::printTreeIO(TNode* node) {
	if (node == nullptr) return;

	if (node->left != nullptr) {
		printTreeIO(node->left);
	}

	node->printNode();

	if (node->right != nullptr) {
		printTreeIO(node->right);
	}
}

void BST::printTreePost(TNode* node) {
	if (node == nullptr) return;

	if (node->left != nullptr) {
		printTreePost(node->left);
	}

	if (node->right != nullptr) {
		printTreePost(node->right);
	}

	node->printNode();
}

void BST::printTreePre(TNode* node) {
	if (node == nullptr) return;

	node->printNode();
	if (node->left != nullptr) {
		printTreePre(node->left);
	}

	if (node->right != nullptr) {
		printTreePre(node->right);
	}
}

TNode* BST::remove(StudInf* data) {
	TNode* node = find(data);

	if (node->left == nullptr && node->right == nullptr) {
		return removeNoKids(node);
	} else if (node->left != nullptr && node->right == nullptr) {
		return removeOneKid(node, true);
	} else if (node->right != nullptr && node->left == nullptr) {
		return removeOneKid(node, false);
	} else {
		TNode* child = node->left;
		while(child->right != nullptr) {
			child = child->right;
		}
		if (child != node->left) {
			child->left = node->left;
		} else {
			child->left = node->left->left;
		}
		child->right = node->right;

		child->parent->right = nullptr;

		child->parent = node->parent;

		if (child->parent == nullptr) {
			root = child;
		}

		if (node->parent != nullptr && node->parent->left == node) {
			node->parent->left = child;
		} else if (node->parent != nullptr && node->parent->right == node) {
			node->parent->right = child;
		}

		node->left = nullptr;
		node->right = nullptr;
		node->parent = nullptr;
		return node;
	}
}

TNode* BST::removeNoKids(TNode* node) {
	TNode* parent = (node->parent == nullptr) ? nullptr : node->parent;
	
	if (parent != nullptr && parent->left == node) {
		parent->left = nullptr;
	}
	
	if (parent != nullptr && parent->right == node) {
		parent->right = nullptr;
	}

	node->parent = nullptr;
	return node;
}

TNode* BST::removeOneKid(TNode* node, bool leftFlag) {
	TNode* parent = (node->parent == nullptr) ? nullptr : node->parent;

	if (parent != nullptr && parent->left == node) {
		TNode* child = (leftFlag) ? node->left : node->right;
		parent->left = child;
		child->parent = parent;
	} else if (parent != nullptr && parent->right == node) {
		TNode* child = (leftFlag) ? node->left : node->right;
		parent->right = child;
		child->parent = parent;
	}

	node->parent = nullptr;
	node->left = nullptr;
	node->right = nullptr;
	return node;
}

TNode* BST::remove(string l, string f) {
	std::string sarr[6] = {l, "", "", f, "", ""};
	return remove(new StudInf(sarr));
}

int BST::getBalance(TNode *tmp) {
	if (tmp == nullptr) {
		return 0;
	}

	if (tmp->left != nullptr && tmp->right != nullptr) {
		return tmp->left->height - tmp->right->height;
	} else if (tmp->left != nullptr) {
		return tmp->left->height;
	} else if (tmp->right != nullptr) {
		return tmp->right->height;
	} else {
		return 0;
	}
}

void BST::rebalance(TNode *start) {
	if (start == nullptr) {
		return;
	}

	if (getBalance(start) == 2) {
		if (getBalance(start->left) == 1) {
			rotateRight(start->left);
		} else if (getBalance(start->left) == -1) {
			rotateLeft(start->left);
			rotateRight(start);
		}
	} else if (getBalance(start) == -2) {
		if (getBalance(start->right) == 1) {
			rotateRight(start->right);
			rotateLeft(start);
		} else if (getBalance(start->right) == -1) {
			rotateLeft(start->right);
		}
	} else {
		if (start->parent != nullptr) {
			rebalance(start->parent);
		}
	}
}

TNode* BST::rotateRight(TNode *tmp) {
	if (tmp == nullptr) {
		return nullptr;
	}

	TNode* top = tmp->left;
	if (top == nullptr) {
		return nullptr;
	}
	tmp->left = top->right;
	if (tmp->left != nullptr) {
		tmp->left->parent = tmp;
	}

	if (tmp->parent != nullptr) {
		if (tmp->parent->left == tmp) {
			tmp->parent->left = top;
		} else {
			tmp->parent->right = top;
		}
	}

	top->parent = tmp->parent;
	top->right = tmp;
	tmp->parent = top;

	if (top->parent == nullptr) {
		root = top;
	}

	setHeight();

	return top;
}

TNode* BST::rotateLeft(TNode *tmp) {
	if (tmp == nullptr) {
		return nullptr;
	}

	TNode* top = tmp->right;
	if (top == nullptr) {
		return nullptr;
	}
	tmp->right = top->left;
	if (tmp->right != nullptr) {
		tmp->right->parent = tmp;
	}

	if (tmp->parent != nullptr) {
		if (tmp->parent->left == tmp) {
			tmp->parent->left = top;
		} else {
			tmp->parent->right = top;
		}
	}

	top->parent = tmp->parent;
	top->left = tmp;
	tmp->parent = top;

	if (top->parent == nullptr) {
		root = top;
	}

	setHeight();

	return top;
}
