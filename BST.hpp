/*
 * BST.hpp
 *
 *  Created on: Apr 10, 2020
 *      Author: 13027
 */

#ifndef BST_HPP_
#define BST_HPP_

	#include <string>

	#include "TNode.hpp"

	class BST {
		TNode *root;

	public:
		BST();
		BST(StudInf* s);
		BST(string sarr[]);  // updated

		bool insert(StudInf* s);
		bool insert(StudInf* s, TNode* start);
		bool insert(string sarr[]);  //updated

		TNode *find(string l, string f); //updated
		TNode* find(StudInf* s);
		TNode* find(StudInf* s, TNode* start);

		void printTreeIO();
		void printTreeIO(TNode *n);
		void printTreePre();
		void printTreePre(TNode *n);
		void printTreePost();
		void printTreePost(TNode *n);

		void clearTree();
		void clearTree(TNode *tmp);

		TNode *removeNoKids(TNode *tmp);
		TNode *removeOneKid(TNode *tmp, bool leftFlag);
		TNode *remove(StudInf* s);
		TNode *remove(string s, string l);  //updated

		int setHeight(TNode *n);
		int setHeight();

		/******************New for AVL ***************************/
		void rebalance(TNode *start);
		int getBalance(TNode *tmp);
		TNode *rotateRight(TNode *tmp);
		TNode *rotateLeft(TNode *tmp);
	};

#endif /* BST_HPP_ */
